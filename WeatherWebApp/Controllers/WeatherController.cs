﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using NPOI.OpenXml4Net.Exceptions;
using WeatherWebApp.Models;
using WeatherWebApp.Common;
using WeatherWebApp.Common.Serialization;

namespace WeatherWebApp.Controllers
{
    public class WeatherController : Controller
    {
        private const int DefaultPageSize = 5;

        private readonly WeatherDbContext dbContext;
        private readonly int pageSize;

        public WeatherController(WeatherDbContext dbContext, IConfiguration configuration)
        {
            this.dbContext = dbContext;

            if (!int.TryParse(configuration["ViewSettings:ItemsPerPage"], out pageSize) || pageSize < 1)
            {
                pageSize = DefaultPageSize;
            }
        }

        [Route("Weather/Yearly/{Page?}")]
        public IActionResult Yearly(int page = 1)
        {
            IEnumerable<WeatherViewModel> averageWeather =
                dbContext.Weather
                .AsNoTracking()
                .AverageDataForGroupByYears()
                .Skip((page - 1) * pageSize)
                .Take(pageSize);

            return View("Index", 
                new WeatherListViewModel()
                {
                    PagingInfo = new PagingInfo()
                    {
                        CurrentPage = page,
                        ItemsPerPage = pageSize,
                        TotalItems = dbContext.Weather
                            .Select(w => w.DateTime.Year)
                            .Distinct()
                            .Count()
                    },
                    WeatherList = averageWeather
                });
        }

        [Route("Weather/Monthly/{Page?}")]
        public IActionResult Monthly(int page = 1)
        {
            IEnumerable<WeatherViewModel> averageWeather =
                dbContext.Weather
                    .AsNoTracking()
                    .AverageDataForGroupByMonths()
                    .Skip((page - 1) * pageSize)
                    .Take(pageSize);

            return View("Index",
                new WeatherListViewModel()
                {
                    PagingInfo = new PagingInfo()
                    {
                        CurrentPage = page,
                        ItemsPerPage = pageSize,
                        TotalItems = dbContext.Weather
                            .Select(w => new { w.DateTime.Year, w.DateTime.Month })
                            .Distinct()
                            .Count()
                    },
                    WeatherList = averageWeather
                });
        }

        [HttpGet]
        public IActionResult Upload()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Upload(IFormFile[] files)
        {
            if (files.Length == 0)
            {
                return View(nameof(Upload), new FileUploadViewModel() { IsSuccessful = false, Message = "Не был загружен ни один файл" });
            }

            string currentFileName = string.Empty;
            try
            {
                foreach (IFormFile file in files)
                {
                    currentFileName = file.FileName;
                    await using (Stream fileStream = file.OpenReadStream())
                    {
                        IEnumerable<Weather> fileData = ExcelSerializer.DeserializeExcel(fileStream);
                        await dbContext.AddRangeAsync(fileData);
                    }
                }
            }
            catch (InvalidFormatException)
            {
                return View(nameof(Upload), new FileUploadViewModel() { IsSuccessful = false, Message = $"Загруженный файл {currentFileName} имеет неправильный формат" });
            }

            await dbContext.SaveChangesAsync();
            return View(nameof(Upload), new FileUploadViewModel() { IsSuccessful = true, Message = "Данные успешно загружены в базу данных" });
        }

    }
}