﻿namespace WeatherWebApp.Models
{
    public enum WeatherDataTableColumns
    {
        Date = 0,
        Time = 1,
        Temperature = 2,
        Humidity = 3,
        DewPoint = 4,
        AtmospherePressure = 5,
        WindDirection = 6,
        WindSpeed = 7,
        Cloudiness = 8,
        CloudBase = 9,
        HorizontalVisibility = 10,
        WeatherConditions = 11
    }
}