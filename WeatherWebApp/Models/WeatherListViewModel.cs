﻿using System.Collections.Generic;

namespace WeatherWebApp.Models
{
    public class WeatherListViewModel
    {
        public IEnumerable<WeatherViewModel> WeatherList { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}