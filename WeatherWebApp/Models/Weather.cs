﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Npgsql.PostgresTypes;
using Org.BouncyCastle.Asn1.Cms;

namespace WeatherWebApp.Models
{
    public class Weather
    {
        public long WeatherId { get; set; }

        [Npoi.Mapper.Attributes.Column((ushort) WeatherDataTableColumns.Date, CustomFormat = "dd.MM.yyyy")]
        [NotMapped]
        public DateTime Date { get; set; }

        [Npoi.Mapper.Attributes.Column((ushort) WeatherDataTableColumns.Time, CustomFormat = "hh:mm")]
        [NotMapped]
        public DateTime Time { get; set; }

        public DateTime DateTime
        {
            get => new DateTime(Date.Year, Date.Month, Date.Day,
                Time.Hour, Time.Minute, Time.Second);
            set
            {
                Date = value.Date;
                Time = default(DateTime).Add(value.TimeOfDay);
            }
        }
        

        [Npoi.Mapper.Attributes.Column((ushort) WeatherDataTableColumns.Temperature)]
        public double Temperature { get; set; }

        [Npoi.Mapper.Attributes.Column((ushort) WeatherDataTableColumns.Humidity)]
        public double Humidity { get; set; }

        [Npoi.Mapper.Attributes.Column((ushort) WeatherDataTableColumns.DewPoint)]
        public double DewPoint { get; set; }

        [Npoi.Mapper.Attributes.Column((ushort) WeatherDataTableColumns.AtmospherePressure)]
        public int AtmospherePressure { get; set; }

        [Npoi.Mapper.Attributes.Column((ushort) WeatherDataTableColumns.WindDirection)]
        [StringLength(6)]
        public string WindDirection { get; set; }

        [Npoi.Mapper.Attributes.Column((ushort) WeatherDataTableColumns.WindSpeed)]
        public int WindSpeed { get; set; }

        [Npoi.Mapper.Attributes.Column((ushort) WeatherDataTableColumns.Cloudiness)]
        public int? Cloudiness { get; set; }

        [Npoi.Mapper.Attributes.Column((ushort) WeatherDataTableColumns.CloudBase)]
        public int CloudBase { get; set; }

        [Npoi.Mapper.Attributes.Column((ushort) WeatherDataTableColumns.HorizontalVisibility)]
        public int? HorizontalVisibility { get; set; }

        [Npoi.Mapper.Attributes.Column((ushort) WeatherDataTableColumns.WeatherConditions)]
        public string WeatherConditions { get; set; }
    }
}