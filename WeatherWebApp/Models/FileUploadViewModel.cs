﻿namespace WeatherWebApp.Models
{
    public class FileUploadViewModel
    {
        public bool IsSuccessful { get; set; }
        public string Message { get; set; }
    }
}