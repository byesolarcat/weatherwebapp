﻿using Microsoft.EntityFrameworkCore;

namespace WeatherWebApp.Models
{
    public class WeatherDbContext : DbContext
    {
        public WeatherDbContext(DbContextOptions<WeatherDbContext> options)
            : base(options)
        {

        }

        public virtual DbSet<Weather> Weather { get; set; }
    }
}