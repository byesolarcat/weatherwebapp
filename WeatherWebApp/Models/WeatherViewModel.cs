﻿using System;

namespace WeatherWebApp.Models
{
    public class WeatherViewModel
    {
        public int Year { get; set; }
        public int? Month { get; set; }


        public double Temperature { get; set; }

        public double Humidity { get; set; }

        public double DewPoint { get; set; }

        public double AtmospherePressure { get; set; }
        
        public string WindDirection { get; set; }

        public double WindSpeed { get; set; }

        public double Cloudiness { get; set; }

        public double CloudBase { get; set; }

        public double HorizontalVisibility { get; set; }

        public string WeatherConditions { get; set; }
    }
}