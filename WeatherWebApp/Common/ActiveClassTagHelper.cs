﻿using System;
using Microsoft.AspNetCore.Mvc.TagHelpers;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Microsoft.AspNetCore.Routing;

namespace WeatherWebApp.Common
{
    [HtmlTargetElement(Attributes = "is-active-route")]
    public class ActiveClassTagHelper : AnchorTagHelper
    {
        public ActiveClassTagHelper(IHtmlGenerator generator)
            : base(generator)
        {
        }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            string currentAction = ViewContext.RouteData.Values["action"] as string;
            
            if (Action == currentAction)
            {
                string existingClasses = output.Attributes["class"].Value.ToString();
                if (output.Attributes["class"] != null)
                {
                    output.Attributes.Remove(output.Attributes["class"]);
                }

                output.Attributes.Add("class", $"{existingClasses} active");
            }
        }
    }
}