﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Npoi.Mapper;
using WeatherWebApp.Models;

namespace WeatherWebApp.Common.Serialization
{
    public static class ExcelSerializer
    {
        private const int WeatherTableHeaderOffset = 4;
        private const int WeatherDataSheetsAmount = 12;

        public static IEnumerable<Weather> DeserializeExcel(Stream fileStream)
        {
            Mapper mapper = new Mapper(fileStream);
            mapper.FirstRowIndex = WeatherTableHeaderOffset;

            List<Weather> weatherData = new List<Weather>();
            for (int sheetIndex = 0; sheetIndex < WeatherDataSheetsAmount; sheetIndex++)
            {
                IEnumerable<Weather> sheetData = mapper.Take<Weather>(sheetIndex)
                                                       .Select(row => row.Value);
                
                weatherData.AddRange(sheetData);
            }

            return weatherData;
        }
    }
}
