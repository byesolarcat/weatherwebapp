﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WeatherWebApp.Models;

namespace WeatherWebApp.Common
{
    public static class QueryableExtensions
    {
        public static IEnumerable<WeatherViewModel> AverageDataForGroupByYears(this IQueryable<Weather> source, int skip = 0, int take = int.MaxValue)
        {
            IEnumerable<WeatherViewModel> averageData = source
                .GroupBy(w => w.DateTime.Year)
                .Select(g => new WeatherViewModel()
                {
                    Year = g.Key,
                    Temperature = g.Average(k => k.Temperature),
                    Humidity = g.Average(k => k.Humidity),
                    DewPoint = g.Average(k => k.DewPoint),
                    AtmospherePressure = g.Average(k => k.AtmospherePressure),
                    WindSpeed = g.Average(k => k.WindSpeed),
                    Cloudiness = g.Average(k => k.Cloudiness ?? 0),
                    CloudBase = g.Average(k => k.CloudBase),
                    HorizontalVisibility = g.Average(k => k.HorizontalVisibility ?? 0)
                })
                .OrderByDescending(w => w.Year)
                .Skip(skip)
                .Take(take)
                .ToList();

            foreach (WeatherViewModel weather in averageData)
            {
                weather.WindDirection = source
                    .Where(w => w.DateTime.Year == weather.Year)
                    .Select(w => w.WindDirection)
                    .MostFrequentEntry(wd => wd);

                weather.WeatherConditions = source
                    .Where(w => w.DateTime.Year == weather.Year)
                    .Select(w => w.WeatherConditions)
                    .MostFrequentEntry(wc => wc);
            }

            return averageData;
        }

        public static IEnumerable<WeatherViewModel> AverageDataForGroupByMonths(this IQueryable<Weather> source, int skip = 0, int take = int.MaxValue)
        {
            IEnumerable<WeatherViewModel> averageData = source
                .GroupBy(w => new { w.DateTime.Year, w.DateTime.Month })
                .Select(g => new WeatherViewModel()
                {
                    Year = g.Key.Year,
                    Month = g.Key.Month,
                    Temperature = g.Average(k => k.Temperature),
                    Humidity = g.Average(k => k.Humidity),
                    DewPoint = g.Average(k => k.DewPoint),
                    AtmospherePressure = g.Average(k => k.AtmospherePressure),
                    WindSpeed = g.Average(k => k.WindSpeed),
                    Cloudiness = g.Average(k => k.Cloudiness ?? 0),
                    CloudBase = g.Average(k => k.CloudBase),
                    HorizontalVisibility = g.Average(k => k.HorizontalVisibility ?? 0)
                })
                .OrderByDescending(w => w.Year)
                .ThenByDescending(w => w.Month)
                .Skip(skip)
                .Take(take)
                .ToList();

            foreach (WeatherViewModel weather in averageData)
            {
                weather.WindDirection = source
                    .Where(w => w.DateTime.Year == weather.Year
                                && w.DateTime.Month == weather.Month)
                    .Select(w => w.WindDirection)
                    .MostFrequentEntry(wd => wd);

                weather.WeatherConditions = source
                    .Where(w => w.DateTime.Year == weather.Year
                                && w.DateTime.Month == weather.Month)
                    .Select(w => w.WeatherConditions)
                    .MostFrequentEntry(wc => wc);
            }

            return averageData;
        }
    }
}