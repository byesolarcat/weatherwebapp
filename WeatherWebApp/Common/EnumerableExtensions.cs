﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WeatherWebApp.Common
{
    public static class EnumerableExtensions
    {
        public static TEntry MostFrequentEntry<TSource, TEntry>(this IEnumerable<TSource> source, Func<TSource, TEntry> propertyFunc)
        {
            return source.GroupBy(propertyFunc)
                .OrderByDescending(g => g.Count())
                .Select(g => g.Key)
                .FirstOrDefault(k => k != null && !string.IsNullOrWhiteSpace(k.ToString()));
        }
    }
}