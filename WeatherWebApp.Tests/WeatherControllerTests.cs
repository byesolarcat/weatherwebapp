﻿using System;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Moq;
using Moq.EntityFrameworkCore;
using WeatherWebApp.Controllers;
using WeatherWebApp.Models;
using WeatherWebApp.Tests.TestCaseData;
using Xunit;

namespace WeatherWebApp.Tests
{
    public class WeatherControllerTests
    {
        [Theory]
        [ClassData(typeof(WeatherTestCaseDataByMonths))]
        public void TestMonthly_Pagination(Weather[] testData, WeatherViewModel[] groupedTestData)
        {
            const int itemsPerPage = 2;
            Mock<WeatherDbContext> dbContext = new Mock<WeatherDbContext>(new DbContextOptions<WeatherDbContext>());
            dbContext.Setup(db => db.Weather).ReturnsDbSet(testData);
            Mock<IConfiguration> config = new Mock<IConfiguration>();
            config.Setup(c => c["ViewSettings:ItemsPerPage"]).Returns(itemsPerPage.ToString());

            WeatherController controller = new WeatherController(dbContext.Object, config.Object);
            var model = (controller.Monthly() as ViewResult)!.ViewData.Model as WeatherListViewModel;

            Assert.Equal(itemsPerPage, model!.PagingInfo.ItemsPerPage);
            Assert.Equal(groupedTestData.Length, model!.PagingInfo.TotalItems);
            Assert.Equal(Math.Ceiling((decimal) groupedTestData.Length / itemsPerPage), model!.PagingInfo.TotalPages);
        }

        [Theory]
        [ClassData(typeof(WeatherTestCaseDataByYears))]
        public void TestYearly_Pagination(Weather[] testData, WeatherViewModel[] groupedTestData)
        {
            const int itemsPerPage = 2;
            Mock<WeatherDbContext> dbContext = new Mock<WeatherDbContext>(new DbContextOptions<WeatherDbContext>());
            dbContext.Setup(db => db.Weather).ReturnsDbSet(testData);
            Mock<IConfiguration> config = new Mock<IConfiguration>();
            config.Setup(c => c["ViewSettings:ItemsPerPage"]).Returns(itemsPerPage.ToString());

            WeatherController controller = new WeatherController(dbContext.Object, config.Object);
            var model = (controller.Yearly() as ViewResult)!.ViewData.Model as WeatherListViewModel;

            Assert.Equal(itemsPerPage, model!.PagingInfo.ItemsPerPage);
            Assert.Equal(groupedTestData.Length, model!.PagingInfo.TotalItems);
            Assert.Equal(Math.Ceiling((decimal)groupedTestData.Length / itemsPerPage), model!.PagingInfo.TotalPages);
        }

        [Fact]
        public async void TestUpload_NoFiles_NotSuccessful()
        {
            Mock<WeatherDbContext> dbContext = new Mock<WeatherDbContext>(new DbContextOptions<WeatherDbContext>());
            Mock<IConfiguration> config = new Mock<IConfiguration>();

            WeatherController controller = new WeatherController(dbContext.Object, config.Object);
            var model = (await controller.Upload(Array.Empty<IFormFile>()) as ViewResult)!.ViewData.Model as FileUploadViewModel;

            Assert.False(model!.IsSuccessful);
        }

        [Fact]
        public async void TestUpload_WrongFormatFile_NotSuccessful()
        {
            Mock<WeatherDbContext> dbContext = new Mock<WeatherDbContext>(new DbContextOptions<WeatherDbContext>());
            Mock<IConfiguration> config = new Mock<IConfiguration>();
            Mock<IFormFile> streamMock = new Mock<IFormFile>();
            byte[] randomBytes = new byte[] { 0, 1, 2, 3, 4, 5 };
            streamMock.Setup(s => s.OpenReadStream()).Returns(new MemoryStream(randomBytes));

            WeatherController controller = new WeatherController(dbContext.Object, config.Object);
            var model = (await controller.Upload(new IFormFile[] { streamMock.Object }) as ViewResult)!.ViewData.Model as FileUploadViewModel;

            Assert.False(model!.IsSuccessful);
        }
    }
}