﻿using System;
using System.Collections;
using System.Collections.Generic;
using WeatherWebApp.Models;

namespace WeatherWebApp.Tests.TestCaseData
{
    public class WeatherTestCaseDataByYears : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return new object[]
            {
                new Weather[]
                {
                    new Weather()
                    {
                        DateTime = new DateTime(2019, 1, 1),
                        Temperature = 10,
                        Humidity = 10,
                        DewPoint = 10,
                        AtmospherePressure = 10,
                        WindDirection = "N/W",
                        WindSpeed = 10,
                        Cloudiness = 10,
                        CloudBase = 10,
                        HorizontalVisibility = 10,
                        WeatherConditions = "Sunny"
                    }
                },
                new WeatherViewModel[]
                {
                    new WeatherViewModel()
                    {
                        Year = 2019,
                        Temperature = 10,
                        Humidity = 10,
                        DewPoint = 10,
                        AtmospherePressure = 10,
                        WindDirection = "N/W",
                        WindSpeed = 10,
                        Cloudiness = 10,
                        CloudBase = 10,
                        HorizontalVisibility = 10,
                        WeatherConditions = "Sunny"
                    }
                }
            };

            yield return new object[]
            {
                new Weather[]
                {
                    new Weather()
                    {
                        DateTime = new DateTime(2019, 1, 1),
                        Temperature = 10,
                        Humidity = 10,
                        DewPoint = 10,
                        AtmospherePressure = 10,
                        WindDirection = "N/W",
                        WindSpeed = 10,
                        Cloudiness = 10,
                        CloudBase = 10,
                        HorizontalVisibility = 10,
                        WeatherConditions = "Sunny"
                    },
                    new Weather()
                    {
                        DateTime = new DateTime(2020, 6, 6),
                        Temperature = 5,
                        Humidity = 5,
                        DewPoint = 5,
                        AtmospherePressure = 5,
                        WindDirection = "S/W",
                        WindSpeed = 5,
                        Cloudiness = 5,
                        CloudBase = 5,
                        HorizontalVisibility = 5,
                        WeatherConditions = "Cloudy"
                    },
                },
                new WeatherViewModel[]
                {
                    new WeatherViewModel()
                    {
                        Year = 2020,
                        Temperature = 5,
                        Humidity = 5,
                        DewPoint = 5,
                        AtmospherePressure = 5,
                        WindDirection = "S/W",
                        WindSpeed = 5,
                        Cloudiness = 5,
                        CloudBase = 5,
                        HorizontalVisibility = 5,
                        WeatherConditions = "Cloudy"
                    },
                    new WeatherViewModel()
                    {
                        Year = 2019,
                        Temperature = 10,
                        Humidity = 10,
                        DewPoint = 10,
                        AtmospherePressure = 10,
                        WindDirection = "N/W",
                        WindSpeed = 10,
                        Cloudiness = 10,
                        CloudBase = 10,
                        HorizontalVisibility = 10,
                        WeatherConditions = "Sunny"
                    }
                }
            };

            yield return new object[]
            {
                new Weather[]
                {
                    new Weather()
                    {
                        DateTime = new DateTime(2019, 1, 1),
                        Temperature = 10,
                        Humidity = 10,
                        DewPoint = 10,
                        AtmospherePressure = 10,
                        WindDirection = "N/W",
                        WindSpeed = 10,
                        Cloudiness = 10,
                        CloudBase = 10,
                        HorizontalVisibility = 10,
                        WeatherConditions = "Sunny"
                    },
                    new Weather()
                    {
                        DateTime = new DateTime(2019, 3, 3),
                        Temperature = 0,
                        Humidity = 0,
                        DewPoint = 0,
                        AtmospherePressure = 0,
                        WindDirection = "N/W",
                        WindSpeed = 0,
                        Cloudiness = 0,
                        CloudBase = 0,
                        HorizontalVisibility = 0,
                        WeatherConditions = "Sunny"
                    },
                    new Weather()
                    {
                        DateTime = new DateTime(2019, 6, 6),
                        Temperature = 5,
                        Humidity = 5,
                        DewPoint = 5,
                        AtmospherePressure = 5,
                        WindDirection = "S/W",
                        WindSpeed = 5,
                        Cloudiness = 5,
                        CloudBase = 5,
                        HorizontalVisibility = 5,
                        WeatherConditions = "Cloudy"
                    },
                    new Weather()
                    {
                        DateTime = new DateTime(2020, 1, 1),
                        Temperature = 10,
                        Humidity = 10,
                        DewPoint = 10,
                        AtmospherePressure = 10,
                        WindDirection = "N/W",
                        WindSpeed = 10,
                        Cloudiness = 10,
                        CloudBase = 10,
                        HorizontalVisibility = 10,
                        WeatherConditions = "Sunny"
                    },
                    new Weather()
                    {
                        DateTime = new DateTime(2020, 3, 3),
                        Temperature = 0,
                        Humidity = 0,
                        DewPoint = 0,
                        AtmospherePressure = 0,
                        WindDirection = "N/W",
                        WindSpeed = 0,
                        Cloudiness = 0,
                        CloudBase = 0,
                        HorizontalVisibility = 0,
                        WeatherConditions = "Sunny"
                    },
                    new Weather()
                    {
                        DateTime = new DateTime(2020, 6, 6),
                        Temperature = 5,
                        Humidity = 5,
                        DewPoint = 5,
                        AtmospherePressure = 5,
                        WindDirection = "S/W",
                        WindSpeed = 5,
                        Cloudiness = 5,
                        CloudBase = 5,
                        HorizontalVisibility = 5,
                        WeatherConditions = "Cloudy"
                    },
                },
                new WeatherViewModel[]
                {
                    new WeatherViewModel()
                    {
                        Year = 2020,
                        Temperature = 5,
                        Humidity = 5,
                        DewPoint = 5,
                        AtmospherePressure = 5,
                        WindDirection = "N/W",
                        WindSpeed = 5,
                        Cloudiness = 5,
                        CloudBase = 5,
                        HorizontalVisibility = 5,
                        WeatherConditions = "Sunny"
                    },
                    new WeatherViewModel()
                    {
                        Year = 2019,
                        Temperature = 5,
                        Humidity = 5,
                        DewPoint = 5,
                        AtmospherePressure = 5,
                        WindDirection = "N/W",
                        WindSpeed = 5,
                        Cloudiness = 5,
                        CloudBase = 5,
                        HorizontalVisibility = 5,
                        WeatherConditions = "Sunny"
                    },
                }
            };
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}