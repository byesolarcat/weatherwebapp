﻿using System;
using System.Collections.Generic;
using WeatherWebApp.Models;

namespace WeatherWebApp.Tests.TestCaseData
{
    public class WeatherViewModelEqualityComparer : IEqualityComparer<WeatherViewModel>
    {
        public bool Equals(WeatherViewModel x, WeatherViewModel y)
        {
            if (ReferenceEquals(x, y)) return true;
            if (ReferenceEquals(x, null)) return false;
            if (ReferenceEquals(y, null)) return false;
            if (x.GetType() != y.GetType()) return false;
            return x.Year == y.Year && x.Month == y.Month && x.Temperature.Equals(y.Temperature) && x.Humidity.Equals(y.Humidity) && x.DewPoint.Equals(y.DewPoint) && x.AtmospherePressure.Equals(y.AtmospherePressure) && x.WindDirection == y.WindDirection && x.WindSpeed.Equals(y.WindSpeed) && x.Cloudiness.Equals(y.Cloudiness) && x.CloudBase.Equals(y.CloudBase) && x.HorizontalVisibility.Equals(y.HorizontalVisibility) && x.WeatherConditions == y.WeatherConditions;
        }

        public int GetHashCode(WeatherViewModel obj)
        {
            HashCode hashCode = new HashCode();
            hashCode.Add(obj.Year);
            hashCode.Add(obj.Month);
            hashCode.Add(obj.Temperature);
            hashCode.Add(obj.Humidity);
            hashCode.Add(obj.DewPoint);
            hashCode.Add(obj.AtmospherePressure);
            hashCode.Add(obj.WindDirection);
            hashCode.Add(obj.WindSpeed);
            hashCode.Add(obj.Cloudiness);
            hashCode.Add(obj.CloudBase);
            hashCode.Add(obj.HorizontalVisibility);
            hashCode.Add(obj.WeatherConditions);
            return hashCode.ToHashCode();
        }
    }
}