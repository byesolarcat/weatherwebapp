using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using WeatherWebApp.Common;
using WeatherWebApp.Models;
using WeatherWebApp.Tests.TestCaseData;
using Xunit;

namespace WeatherWebApp.Tests
{
    public class EnumerableExtensionsTests
    {
        [Fact]
        public void MostFrequentEntry_ArrayOfIntegers_ReturnsMostFrequentInteger()
        {
            int[] elements = {1, 2, 2, 3, 4, 5};
            const int expected = 2;

            int actual = elements.MostFrequentEntry(i => i);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void MostFrequentEntry_EntriesWithSameFrequency_ReturnsFirst()
        {
            int[] elements = { 1, 2, 2, 3, 4, 55 };
            const int expected = 2;

            int actual = elements.MostFrequentEntry(i => i);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void MostFrequentEntry_ArrayOfWeather_ReturnsMostFrequentNotNullWindDirection()
        {
            Weather[] elements =
            {
                new Weather() {WindDirection = "N/W"},
                new Weather() {WindDirection = "N/W"},
                new Weather() {WindDirection = "S/W"},
                new Weather() {WindDirection = null},
                new Weather() {WindDirection = null},
                new Weather() {WindDirection = null},
                new Weather() {WindDirection = null},
                new Weather() {WindDirection = null},
                new Weather() {WindDirection = "S/W"},
                new Weather() {WindDirection = "S/W"},
            };
            const string expected = "S/W";

            string actual = elements.MostFrequentEntry(w => w.WindDirection);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void MostFrequentEntry_ArrayOfWeather_ReturnsMostFrequentNotNullWeatherConditions()
        {
            Weather[] elements =
            {
                new Weather() {WeatherConditions = "Cloudy"},
                new Weather() {WeatherConditions = "Cloudy"},
                new Weather() {WeatherConditions = "Cloudy"},
                new Weather() {WeatherConditions = null},
                new Weather() {WeatherConditions = null},
                new Weather() {WeatherConditions = null},
                new Weather() {WeatherConditions = null},
                new Weather() {WeatherConditions = null},
                new Weather() {WeatherConditions = "Sunny"},
                new Weather() {WeatherConditions = "Sunny"},
            };
            const string expected = "Cloudy";

            string actual = elements.MostFrequentEntry(w => w.WeatherConditions);

            Assert.Equal(expected, actual);
        }

        [Theory]
        [ClassData(typeof(WeatherTestCaseDataByYears))]
        public void AverageDataForGroupByYears(Weather[] weather, WeatherViewModel[] expected)
        {
            IEnumerable<WeatherViewModel> actual = weather.AsQueryable()
                .AverageDataForGroupByYears().ToArray();
            
            Assert.Equal(expected, actual, new WeatherViewModelEqualityComparer());
        }

        [Theory]
        [ClassData(typeof(WeatherTestCaseDataByMonths))]
        public void AverageDataForGroupByMonths(Weather[] weather, WeatherViewModel[] expected)
        {
            IEnumerable<WeatherViewModel> actual = weather.AsQueryable()
                .AverageDataForGroupByMonths().ToArray();

            Assert.Equal(expected, actual, new WeatherViewModelEqualityComparer());
        }
    }
}
